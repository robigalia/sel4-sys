/*
 * Copyright 2015, Killian Coddington
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 */

pub const seL4_DataFault: usize = 0;
pub const seL4_InstructionFault: usize = 1;

pub const seL4_WordBits: usize = 32;
pub const seL4_WordSizeBits: usize = 2;
pub const seL4_PageBits: usize = 12;
pub const seL4_LargePageBits: usize = 16;
pub const seL4_SlotBits: usize = 4;
pub const seL4_TCBBits: usize = 9;
pub const seL4_EndpointBits: usize = 4;
pub const seL4_NotificationBits: usize = 5;
pub const seL4_ReplyBits: usize = 4;

#[cfg(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT")]
pub const seL4_PageTableBits: usize = 12;
#[cfg(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT")]
pub const seL4_SectionBits: usize = 21;
#[cfg(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT")]
pub const seL4_SuperSectionBits: usize = 25;
pub const seL4_PGDBits: usize = 5;
pub const seL4_VCPUBits: usize = 12;

#[cfg(not(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT"))]
pub const seL4_PageTableBits: usize = 10;
#[cfg(not(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT"))]
pub const seL4_SectionBits: usize = 20;
#[cfg(not(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT"))]
pub const seL4_SuperSectionBits: usize = 24;

pub const seL4_PageDirBits: usize = 14;
pub const seL4_ASIDPoolBits: usize = 12;
pub const seL4_IOPageTableBits: usize = 12;

pub const seL4_MinUntypedBits: usize = 4;
pub const seL4_MaxUntypedBits: usize = 29;

pub const seL4_FirstBreakpoint: isize = 0;
pub const seL4_FirstDualFunctionMonitor: isize = -1;
pub const seL4_NumDualFunctionMonitors: usize = 0;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A15", feature = "CONFIG_ARM_CORTEX_A53",
            feature = "CONFIG_ARM_CORTEX_A9", feature = "CONFIG_ARM_CORTEX_A7"))]
pub const seL4_NumHWBreakpoints: usize = 10;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A15", feature = "CONFIG_ARM_CORTEX_A53",
            feature = "CONFIG_ARM_CORTEX_A9", feature = "CONFIG_ARM_CORTEX_A7"))]
pub const seL4_NumExclusiveBreakpoints: usize = 6;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A15", feature = "CONFIG_ARM_CORTEX_A53",
            feature = "CONFIG_ARM_CORTEX_A9", feature = "CONFIG_ARM_CORTEX_A7"))]
pub const seL4_NumExclusiveWatchpoints: usize = 4;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A15", feature = "CONFIG_ARM_CORTEX_A53",
            feature = "CONFIG_ARM_CORTEX_A9", feature = "CONFIG_ARM_CORTEX_A7"))]
pub const seL4_FirstWatchpoints: isize = 6;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A8", feature = "CONFIG_ARM1136JF_S"))]
pub const seL4_NumHWBreakpoints: usize = 8;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A8", feature = "CONFIG_ARM1136JF_S"))]
pub const seL4_NumExclusiveBreakpoints: usize = 6;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A8", feature = "CONFIG_ARM1136JF_S"))]
pub const seL4_NumExclusiveWatchpoints: usize = 2;
#[cfg(any(feature = "CONFIG_ARM_CORTEX_A8", feature = "CONFIG_ARM1136JF_S"))]
pub const seL4_FirstWatchpoints: isize = 6;

pub const seL4_Frame_Args: usize = 4;
pub const seL4_Frame_MRs: usize = 7;
pub const seL4_Frame_HasNPC: usize = 0;

pub const seL4_GlobalsFrame: *mut u8 = 0xffffc000 as *mut u8;

pub type seL4_ARM_Page = seL4_CPtr;
pub type seL4_ARM_PageTable = seL4_CPtr;
pub type seL4_ARM_PageDirectory = seL4_CPtr;
pub type seL4_ARM_ASIDControl = seL4_CPtr;
pub type seL4_ARM_ASIDPool = seL4_CPtr;
pub type seL4_ARM_VCPU = seL4_CPtr;
pub type seL4_ARM_IOSpace = seL4_CPtr;
pub type seL4_ARM_IOPageTable = seL4_CPtr;

error_types!(u32);

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UserContext {
    pub pc: seL4_Word,
    pub sp: seL4_Word,
    pub cpsr: seL4_Word,
    pub r0: seL4_Word,
    pub r1: seL4_Word,
    pub r8: seL4_Word,
    pub r9: seL4_Word,
    pub r10: seL4_Word,
    pub r11: seL4_Word,
    pub r12: seL4_Word,
    pub r2: seL4_Word,
    pub r3: seL4_Word,
    pub r4: seL4_Word,
    pub r5: seL4_Word,
    pub r6: seL4_Word,
    pub r7: seL4_Word,
    pub r14: seL4_Word,
}

#[repr(u32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum seL4_ARM_VMAttributes {
    NoAttributes = 0,
    PageCacheable = 1,
    ParityEnabled = 2,
    PageCacheableParityEnabled = 3,
    ExecuteNever = 4,
    PageCacheableExecuteNever = 5,
    ParityEnabledExecuteNever = 6,
    PageCacheableParityEnabledExecuteNever = 7,
}

pub const Default_VMAttributes: usize = 3;

impl Default for seL4_ARM_VMAttributes {
    fn default() -> seL4_ARM_VMAttributes {
        seL4_ARM_VMAttributes::PageCacheableParityEnabled
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum seL4_ObjectType {
    seL4_UntypedObject = 0,
    seL4_TCBObject,
    seL4_EndpointObject,
    seL4_NotificationObject,
    seL4_CapTableObject,
    seL4_SchedContextObject,
    seL4_ReplyObject,
    seL4_ARM_SmallPageObject,
    seL4_ARM_LargePageObject,
    seL4_ARM_SectionObject,
    seL4_ARM_SuperSectionObject,
    seL4_ARM_PageTableObject,
    seL4_ARM_PageDirectoryObject,
    #[cfg(feature = "CONFIG_ARM_HYPERVISOR_SUPPORT")]
    seL4_ARM_VCPUObject,
    #[cfg(feature = "CONFIG_ARM_SMMU")]
    seL4_ARM_IOPageTableObject,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_VMFault {
    pub ip: seL4_Word,
    pub addr: seL4_Word,
    pub prefetch_fault: seL4_Word,
    pub fsr: seL4_Word,
}

impl seL4_VMFault {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_VMFault, isize) {
        (
            seL4_VMFault {
                ip: seL4_GetMR(index),
                addr: seL4_GetMR(index + 1),
                prefetch_fault: seL4_GetMR(index + 2),
                fsr: seL4_GetMR(index + 3),
            },
            index + 4,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UnknownSyscall {
    pub r0: seL4_Word,
    pub r1: seL4_Word,
    pub r2: seL4_Word,
    pub r3: seL4_Word,
    pub r4: seL4_Word,
    pub r5: seL4_Word,
    pub r6: seL4_Word,
    pub r7: seL4_Word,
    pub fault_ip: seL4_Word,
    pub sp: seL4_Word,
    pub lr: seL4_Word,
    pub cpsr: seL4_Word,
    pub syscall: seL4_Word,
}

impl seL4_UnknownSyscall {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_UnknownSyscall, isize) {
        (
            seL4_UnknownSyscall {
                r0: seL4_GetMR(index),
                r1: seL4_GetMR(index + 1),
                r2: seL4_GetMR(index + 2),
                r3: seL4_GetMR(index + 3),
                r4: seL4_GetMR(index + 4),
                r5: seL4_GetMR(index + 5),
                r6: seL4_GetMR(index + 6),
                r7: seL4_GetMR(index + 7),
                fault_ip: seL4_GetMR(index + 8),
                sp: seL4_GetMR(index + 9),
                lr: seL4_GetMR(index + 10),
                cpsr: seL4_GetMR(index + 11),
                syscall: seL4_GetMR(index + 12),
            },
            index + 13,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UnknownSyscallReply {
    pub r0: seL4_Word,
    pub r1: seL4_Word,
    pub r2: seL4_Word,
    pub r3: seL4_Word,
    pub r4: seL4_Word,
    pub r5: seL4_Word,
    pub r6: seL4_Word,
    pub r7: seL4_Word,
    pub ip: seL4_Word,
    pub sp: seL4_Word,
    pub lr: seL4_Word,
    pub cpsr: seL4_Word,
}

impl seL4_UnknownSyscallReply {
    pub unsafe fn to_ipcbuf(&self, index: isize) -> isize {
        seL4_SetMR(index, self.r0);
        seL4_SetMR(index + 1, self.r1);
        seL4_SetMR(index + 2, self.r2);
        seL4_SetMR(index + 3, self.r3);
        seL4_SetMR(index + 4, self.r4);
        seL4_SetMR(index + 5, self.r5);
        seL4_SetMR(index + 6, self.r6);
        seL4_SetMR(index + 7, self.r7);
        seL4_SetMR(index + 8, self.ip);
        seL4_SetMR(index + 9, self.sp);
        seL4_SetMR(index + 10, self.lr);
        seL4_SetMR(index + 11, self.cpsr);
        index + 12
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UserException {
    pub fault_ip: seL4_Word,
    pub sp: seL4_Word,
    pub cpsr: seL4_Word,
    pub number: seL4_Word,
    pub code: seL4_Word,
}

impl seL4_UserException {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_UserException, isize) {
        (
            seL4_UserException {
                fault_ip: seL4_GetMR(index),
                sp: seL4_GetMR(index + 1),
                cpsr: seL4_GetMR(index + 2),
                number: seL4_GetMR(index + 3),
                code: seL4_GetMR(index + 4),
            },
            index + 5,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UserExceptionReply {
    pub ip: seL4_Word,
    pub sp: seL4_Word,
    pub cpsr: seL4_Word,
}

impl seL4_UserExceptionReply {
    pub unsafe fn to_ipcbuf(&self, index: isize) -> isize {
        seL4_SetMR(index, self.ip);
        seL4_SetMR(index + 1, self.sp);
        seL4_SetMR(index + 2, self.cpsr);
        index + 3
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_VGICMaintenance {
    pub idx: seL4_Word,
}

impl seL4_VGICMaintenance {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_VGICMaintenance, isize) {
        (
            seL4_VGICMaintenance {
                idx: seL4_GetMR(index),
            },
            index + 1,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_VCPUFault {
    pub hsr: seL4_Word,
}

impl seL4_VCPUFault {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_VCPUFault, isize) {
        (
            seL4_VCPUFault {
                hsr: seL4_GetMR(index),
            },
            index + 1,
        )
    }
}

#[cfg(feature = "CONFIG_IPC_BUF_GLOBALS_FRAME")]
#[inline(always)]
pub unsafe fn seL4_GetIPCBuffer() -> *mut seL4_IPCBuffer {
    *(seL4_GlobalsFrame as *mut *mut seL4_IPCBuffer)
}

#[cfg(feature = "CONFIG_IPC_BUF_TPIDRURW")]
#[inline(always)]
pub unsafe fn seL4_GetIPCBuffer() -> *mut seL4_IPCBuffer {
    let reg: seL4_Word;
    asm!("mrc p15, 0, $0, c13, c0, 2" : "=r"(reg));
    reg as *mut seL4_IPCBuffer
}

#[inline(always)]
pub unsafe fn seL4_GetTag() -> seL4_MessageInfo {
    (*seL4_GetIPCBuffer()).tag
}

#[inline(always)]
pub unsafe fn seL4_SetTag(tag: seL4_MessageInfo) {
    (*seL4_GetIPCBuffer()).tag = tag;
}

#[inline(always)]
pub unsafe fn seL4_GetMR(regnum: isize) -> seL4_Word {
    (*seL4_GetIPCBuffer()).msg[regnum as usize]
}

#[inline(always)]
pub unsafe fn seL4_SetMR(regnum: isize, value: seL4_Word) {
    (*seL4_GetIPCBuffer()).msg[regnum as usize] = value;
}

#[inline(always)]
pub unsafe fn seL4_GetUserData() -> seL4_Word {
    (*seL4_GetIPCBuffer()).userData
}

#[inline(always)]
pub unsafe fn seL4_SetUserData(data: seL4_Word) {
    (*seL4_GetIPCBuffer()).userData = data;
}

#[inline(always)]
pub unsafe fn seL4_GetBadge(index: usize) -> seL4_CapData {
    let mut badge: seL4_CapData = ::core::mem::uninitialized();
    badge.set_Badge((*seL4_GetIPCBuffer()).caps_or_badges[index]);
    badge
}

#[inline(always)]
pub unsafe fn seL4_GetCap(index: usize) -> seL4_CPtr {
    (*seL4_GetIPCBuffer()).caps_or_badges[index] as seL4_CPtr
}

#[inline(always)]
pub unsafe fn seL4_SetCap(index: usize, cptr: seL4_CPtr) {
    (*seL4_GetIPCBuffer()).caps_or_badges[index] = cptr as seL4_Word;
}

#[inline(always)]
pub unsafe fn seL4_GetCapReceivePath(
    receiveCNode: *mut seL4_CPtr,
    receiveIndex: *mut seL4_CPtr,
    receiveDepth: *mut seL4_Word,
) {
    let ipcbuffer = seL4_GetIPCBuffer();
    if !receiveCNode.is_null() {
        *receiveCNode = (*ipcbuffer).receiveCNode;
    }

    if !receiveIndex.is_null() {
        *receiveIndex = (*ipcbuffer).receiveIndex;
    }

    if !receiveDepth.is_null() {
        *receiveDepth = (*ipcbuffer).receiveDepth;
    }
}

#[inline(always)]
pub unsafe fn seL4_SetCapReceivePath(
    receiveCNode: seL4_CPtr,
    receiveIndex: seL4_CPtr,
    receiveDepth: seL4_Word,
) {
    let ipcbuffer = seL4_GetIPCBuffer();
    (*ipcbuffer).receiveCNode = receiveCNode;
    (*ipcbuffer).receiveIndex = receiveIndex;
    (*ipcbuffer).receiveDepth = receiveDepth;
}

macro_rules! opt_deref {
    ($name:expr) => {
        if !$name.is_null() {
            *$name
        } else {
            0
        }
    }
}

macro_rules! opt_assign {
    ($loc:expr, $val:expr) => {
        if !$loc.is_null() {
            *$loc = $val;
        }
    }
}

#[inline(always)]
pub unsafe fn arm_sys_send(
    sys: seL4_Word,
    dest: seL4_Word,
    info: seL4_Word,
    mr0: seL4_Word,
    mr1: seL4_Word,
    mr2: seL4_Word,
    mr3: seL4_Word,
) {
    asm!("swi 0"
         :
         : "{r0}" (dest),
           "{r1}" (info),
           "{r2}" (mr0),
           "{r3}" (mr1),
           "{r4}" (mr2),
           "{r5}" (mr3),
           "{r7}" (sys)
         :
         : "volatile");
}

#[inline(always)]
pub unsafe fn arm_sys_send_null(sys: seL4_Word, src: seL4_Word, info: seL4_Word) {
    asm!("swi 0"
         :
         : "{r0}" (src),
           "{r1}" (info),
           "{r7}" (sys)
         :
         : "volatile");
}

#[inline(always)]
pub unsafe fn arm_sys_recv(
    sys: seL4_Word,
    src: seL4_Word,
    out_badge: *mut seL4_Word,
    out_info: *mut seL4_Word,
    out_mr0: *mut seL4_Word,
    out_mr1: *mut seL4_Word,
    out_mr2: *mut seL4_Word,
    out_mr3: *mut seL4_Word,
    reply: seL4_Word,
) {
    asm!("swi 0"
         : "={r0}" (*out_badge),
           "={r1}" (*out_info),
           "={r2}" (*out_mr0),
           "={r3}" (*out_mr1),
           "={r4}" (*out_mr2),
           "={r5}" (*out_mr3)
         : "{r0}" (src),
           "{r6}" (reply),
           "{r7}" (sys)
         : "memory"
         : "volatile");
}

#[inline(always)]
pub unsafe fn arm_sys_send_recv(
    sys: seL4_Word,
    dest: seL4_Word,
    out_badge: *mut seL4_Word,
    info: seL4_Word,
    out_info: *mut seL4_Word,
    in_out_mr0: *mut seL4_Word,
    in_out_mr1: *mut seL4_Word,
    in_out_mr2: *mut seL4_Word,
    in_out_mr3: *mut seL4_Word,
    reply: seL4_Word,
) {
    asm!("swi 0"
         : "={r0}" (*out_badge),
           "={r1}" (*out_info),
           "={r2}" (*in_out_mr0),
           "={r3}" (*in_out_mr1),
           "={r4}" (*in_out_mr2),
           "={r5}" (*in_out_mr3)
         : "{r0}" (dest),
           "{r1}" (info),
           "{r2}" (*in_out_mr0),
           "{r3}" (*in_out_mr1),
           "{r4}" (*in_out_mr2),
           "{r5}" (*in_out_mr3),
           "{r6}" (reply),
           "{r7}" (sys)
         : "memory"
         : "volatile");
}

#[inline(always)]
pub unsafe fn arm_sys_nbsend_recv(
    sys: seL4_Word,
    dest: seL4_Word,
    src: seL4_Word,
    out_badge: *mut seL4_Word,
    info: seL4_Word,
    out_info: *mut seL4_Word,
    in_out_mr0: *mut seL4_Word,
    in_out_mr1: *mut seL4_Word,
    in_out_mr2: *mut seL4_Word,
    in_out_mr3: *mut seL4_Word,
    reply: seL4_Word,
) {
    asm!("swi 0"
         : "={r0}" (*out_badge),
           "={r1}" (*out_info),
           "={r2}" (*in_out_mr0),
           "={r3}" (*in_out_mr1),
           "={r4}" (*in_out_mr2),
           "={r5}" (*in_out_mr3)
         : "{r0}" (src),
           "{r1}" (info),
           "{r2}" (*in_out_mr0),
           "{r3}" (*in_out_mr1),
           "{r4}" (*in_out_mr2),
           "{r5}" (*in_out_mr3),
           "{r6}" (reply),
           "{r7}" (sys),
           "{r8}" (dest)
         : "memory"
         : "volatile");
}

#[inline(always)]
pub unsafe fn arm_sys_null(sys: seL4_Word) {
    asm!("swi 0"
         :
         : "{r7}" (sys)
         :
         : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_Send(dest: seL4_CPtr, msgInfo: seL4_MessageInfo) {
    arm_sys_send(
        SyscallId::Send as seL4_Word,
        dest,
        msgInfo.words[0],
        seL4_GetMR(0),
        seL4_GetMR(1),
        seL4_GetMR(2),
        seL4_GetMR(3),
    );
}

#[inline(always)]
pub unsafe fn seL4_SendWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
) {
    arm_sys_send(
        SyscallId::Send as seL4_Word,
        dest,
        msgInfo.words[0],
        if mr0.is_null() { 0 } else { *mr0 },
        if mr1.is_null() { 0 } else { *mr1 },
        if mr2.is_null() { 0 } else { *mr2 },
        if mr3.is_null() { 0 } else { *mr3 },
    );
}

#[inline(always)]
pub unsafe fn seL4_NBSend(dest: seL4_CPtr, msgInfo: seL4_MessageInfo) {
    arm_sys_send(
        SyscallId::NBSend as seL4_Word,
        dest,
        msgInfo.words[0],
        seL4_GetMR(0),
        seL4_GetMR(1),
        seL4_GetMR(2),
        seL4_GetMR(3),
    );
}
#[inline(always)]
pub unsafe fn seL4_NBSendWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
) {
    arm_sys_send(
        SyscallId::NBSend as seL4_Word,
        dest,
        msgInfo.words[0],
        if mr0.is_null() { 0 } else { *mr0 },
        if mr1.is_null() { 0 } else { *mr1 },
        if mr2.is_null() { 0 } else { *mr2 },
        if mr3.is_null() { 0 } else { *mr3 },
    );
}

#[inline(always)]
pub unsafe fn seL4_Signal(dest: seL4_CPtr) {
    arm_sys_send_null(
        SyscallId::Send as seL4_Word,
        dest,
        seL4_MessageInfo::new(0, 0, 0, 0).words[0],
    );
}

#[inline(always)]
pub unsafe fn seL4_Recv(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    arm_sys_recv(
        SyscallId::Recv as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_RecvWithMRs(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    arm_sys_recv(
        SyscallId::Recv as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(mr1, msg1);
    opt_assign!(mr2, msg2);
    opt_assign!(mr3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBRecv(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    arm_sys_recv(
        SyscallId::NBRecv as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_Wait(src: seL4_CPtr, sender: *mut seL4_Word) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    arm_sys_recv(
        SyscallId::Wait as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        0,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_WaitWithMRs(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    arm_sys_recv(
        SyscallId::Wait as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        0,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(mr1, msg1);
    opt_assign!(mr2, msg2);
    opt_assign!(mr3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBWait(src: seL4_CPtr, sender: *mut seL4_Word) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    arm_sys_recv(
        SyscallId::NBWait as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        0,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}
#[inline(always)]

pub unsafe fn seL4_Call(mut dest: seL4_CPtr, msgInfo: seL4_MessageInfo) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut msg0 = seL4_GetMR(0);
    let mut msg1 = seL4_GetMR(1);
    let mut msg2 = seL4_GetMR(2);
    let mut msg3 = seL4_GetMR(3);

    arm_sys_send_recv(
        SyscallId::Call as seL4_Word,
        dest,
        &mut dest,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        0,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    info
}

#[inline(always)]
pub unsafe fn seL4_CallWithMRs(
    mut dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    if !mr0.is_null() && msgInfo.get_length() > 0 {
        msg0 = *mr0;
    }
    if !mr1.is_null() && msgInfo.get_length() > 1 {
        msg1 = *mr1;
    }
    if !mr2.is_null() && msgInfo.get_length() > 2 {
        msg2 = *mr2;
    }
    if !mr3.is_null() && msgInfo.get_length() > 3 {
        msg3 = *mr3;
    }

    arm_sys_send_recv(
        SyscallId::Call as seL4_Word,
        dest,
        &mut dest,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        0,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(mr1, msg1);
    opt_assign!(mr2, msg2);
    opt_assign!(mr3, msg3);

    info
}

#[inline(always)]
pub unsafe fn seL4_ReplyRecv(
    src: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = seL4_GetMR(0);
    let mut msg1 = seL4_GetMR(1);
    let mut msg2 = seL4_GetMR(2);
    let mut msg3 = seL4_GetMR(3);

    arm_sys_send_recv(
        SyscallId::ReplyRecv as seL4_Word,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_ReplyRecvWithMRs(
    src: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    if !mr0.is_null() && msgInfo.get_length() > 0 {
        msg0 = *mr0;
    }
    if !mr1.is_null() && msgInfo.get_length() > 1 {
        msg1 = *mr1;
    }
    if !mr2.is_null() && msgInfo.get_length() > 2 {
        msg2 = *mr2;
    }
    if !mr3.is_null() && msgInfo.get_length() > 3 {
        msg3 = *mr3;
    }

    arm_sys_send_recv(
        SyscallId::ReplyRecv as seL4_Word,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(mr1, msg1);
    opt_assign!(mr2, msg2);
    opt_assign!(mr3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendRecv(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = seL4_GetMR(0);
    let mut msg1 = seL4_GetMR(1);
    let mut msg2 = seL4_GetMR(2);
    let mut msg3 = seL4_GetMR(3);

    arm_sys_nbsend_recv(
        SyscallId::NBSendRecv as seL4_Word,
        dest,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendRecvWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    if !mr0.is_null() && msgInfo.get_length() > 0 {
        msg0 = *mr0;
    }
    if !mr1.is_null() && msgInfo.get_length() > 1 {
        msg1 = *mr1;
    }
    if !mr2.is_null() && msgInfo.get_length() > 2 {
        msg2 = *mr2;
    }
    if !mr3.is_null() && msgInfo.get_length() > 3 {
        msg3 = *mr3;
    }

    arm_sys_nbsend_recv(
        SyscallId::NBSendRecv as seL4_Word,
        dest,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        reply,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(mr1, msg1);
    opt_assign!(mr2, msg2);
    opt_assign!(mr3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendWait(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_CPtr,
    sender: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = seL4_GetMR(0);
    let mut msg1 = seL4_GetMR(1);
    let mut msg2 = seL4_GetMR(2);
    let mut msg3 = seL4_GetMR(3);

    arm_sys_nbsend_recv(
        SyscallId::NBSendWait as seL4_Word,
        0,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        dest,
    );

    seL4_SetMR(0, msg0);
    seL4_SetMR(1, msg1);
    seL4_SetMR(2, msg2);
    seL4_SetMR(3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendWaitWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    mr1: *mut seL4_Word,
    mr2: *mut seL4_Word,
    mr3: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = ::core::mem::uninitialized();
    let mut badge = ::core::mem::uninitialized();
    let mut msg0 = ::core::mem::uninitialized();
    let mut msg1 = ::core::mem::uninitialized();
    let mut msg2 = ::core::mem::uninitialized();
    let mut msg3 = ::core::mem::uninitialized();

    if !mr0.is_null() && msgInfo.get_length() > 0 {
        msg0 = *mr0;
    }
    if !mr1.is_null() && msgInfo.get_length() > 1 {
        msg1 = *mr1;
    }
    if !mr2.is_null() && msgInfo.get_length() > 2 {
        msg2 = *mr2;
    }
    if !mr3.is_null() && msgInfo.get_length() > 3 {
        msg3 = *mr3;
    }

    arm_sys_nbsend_recv(
        SyscallId::NBSendWait as seL4_Word,
        0,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        &mut msg1,
        &mut msg2,
        &mut msg3,
        dest,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(mr1, msg1);
    opt_assign!(mr2, msg2);
    opt_assign!(mr3, msg3);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_Yield() {
    arm_sys_null(SyscallId::Yield as seL4_Word);
    asm!("" ::: "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_PRINTING")]
pub unsafe fn seL4_DebugPutChar(c: u8) {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;
    let mut unused5 = 0;

    arm_sys_send_recv(
        SyscallId::DebugPutChar as seL4_Word,
        c as seL4_Word,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        &mut unused5,
        0,
    );
}

#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugHalt() -> ! {
    arm_sys_null(SyscallId::DebugHalt as seL4_Word);
    unreachable!()
}

#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugSnapshot() {
    arm_sys_null(SyscallId::DebugSnapshot as seL4_Word);
}

#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugCapIdentify(mut cap: seL4_CPtr) -> u32 {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;

    arm_sys_send_recv(
        SyscallId::DebugCapIdentify as seL4_Word,
        cap as seL4_Word,
        &mut cap,
        0,
        &mut unused0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        0,
    );

    cap as u32
}

// Note: name MUST be NUL-terminated.
#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugNameThread(tcb: seL4_CPtr, name: &[u8]) {
    core::ptr::copy_nonoverlapping(
        name.as_ptr() as *mut u8,
        (&mut (*seL4_GetIPCBuffer()).msg).as_mut_ptr() as *mut u8,
        name.len(),
    );
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;
    let mut unused5 = 0;

    arm_sys_send_recv(
        SyscallId::DebugNameThread as seL4_Word,
        tcb,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        &mut unused5,
        0,
    );
}

#[inline(always)]
#[cfg(feature = "CONFIG_DANGEROUS_CODE_INJECTION")]
pub unsafe fn seL4_DebugRun(userfn: extern "C" fn(*mut u8), userarg: *mut u8) {
    let userfnptr = userfn as *mut ();
    arm_sys_send_null(
        SyscallId::DebugRun as seL4_Word,
        userfnptr as seL4_Word,
        userarg as seL4_Word,
    );
    asm!("" ::: "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkResetLog() -> seL4_Word {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;

    let mut ret = 0;

    arm_sys_send_recv(
        SyscallId::BenchmarkResetLog as seL4_Word,
        0,
        &mut ret,
        0,
        &mut unused0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        0,
    );

    ret
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkFinalizeLog() -> seL4_Word {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;

    let mut index_ret = 0;

    arm_sys_send_recv(
        SyscallId::BenchmarkFinalizeLog as seL4_Word,
        0,
        &mut index_ret,
        0,
        &mut unused0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        0,
    );

    index_ret
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkSetLogBuffer(mut frame_cptr: seL4_Word) -> seL4_Word {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;

    arm_sys_send_recv(
        SyscallId::BenchmarkSetLogBuffer as seL4_Word,
        frame_cptr,
        &mut frame_cptr,
        0,
        &mut unused0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        0,
    );

    frame_cptr
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkNullSyscall() {
    arm_sys_null(SyscallId::BenchmarkNullSyscall as seL4_Word);
    asm!("" ::: "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkFlushCaches() {
    arm_sys_null(SyscallId::BenchmarkFlushCaches as seL4_Word);
    asm!("" ::: "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_BENCHMARK_TRACK_UTILISATION")]
pub unsafe fn seL4_BenchmarkGetThreadUtilization(tcb: seL4_Word) -> benchmark_track_util {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;
    let mut unused5 = 0;

    arm_sys_send_recv(
        SyscallId::BenchmarkGetThreadUtilisation as seL4_Word,
        tcb,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        &mut unused5,
        0,
    );

    benchmark_track_util {
        thread_utilization: seL4_GetMR(0),
        idle_utilization: seL4_GetMR(1),
        total_utilization: seL4_GetMR(2),
    }
}

#[inline(always)]
#[cfg(feature = "CONFIG_BENCHMARK_TRACK_UTILISATION")]
pub unsafe fn seL4_BenchmarkResetThreadUtilization(tcb: seL4_Word) {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;
    let mut unused3 = 0;
    let mut unused4 = 0;
    let mut unused5 = 0;

    arm_sys_send_recv(
        SyscallId::BenchmarkResetThreadUtilisation as seL4_Word,
        tcb,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        &mut unused3,
        &mut unused4,
        &mut unused5,
        0,
    );
}
