/*
 * Copyright 2015, Corey Richardson
 * Copyright 2014, NICTA
 *
 * This software may be distributed and modified according to the terms of
 * the BSD 2-Clause license. Note that NO WARRANTY is provided.
 * See "LICENSE_BSD2.txt" for details.
 *
 * @TAG(NICTA_BSD)
 */

use core::mem::uninitialized;

pub const seL4_DataFault: usize = 0;
pub const seL4_InstructionFault: usize = 0;

pub const seL4_WordBits: usize = 32;
pub const seL4_WordSizeBits: usize = 2;
pub const seL4_PageBits: usize = 12;
pub const seL4_SlotBits: usize = 4;
pub const seL4_TCBBits: usize = 11;
pub const seL4_EndpointBits: usize = 4;
pub const seL4_NotificationBits: usize = 5;
pub const seL4_PageTableBits: usize = 12;
pub const seL4_PageDirBits: usize = 12;
pub const seL4_IOPageTableBits: usize = 12;
pub const seL4_ASIDPoolBits: usize = 12;
pub const seL4_ASIDPoolIndexBits: usize = 10;
pub const seL4_ReplyBits: usize = 4;

pub const seL4_HugePageBits: usize = 30;

pub const seL4_VCPUBits: usize = 14;
pub const seL4_EPTPML4Bits: usize = 12;
pub const seL4_EPTPDPTBits: usize = 12;
pub const seL4_EPTPDBits: usize = 12;
pub const seL4_EPTPTBits: usize = 12;

pub const seL4_PDPTBits: usize = 0;
pub const seL4_LargePageBits: usize = 22;

pub const seL4_MinUntypedBits: usize = 4;
pub const seL4_MaxUntypedBits: usize = 29;

/// When allocating a CPU interrupt vector for an MSI interrupt, program MSI to
/// interrupt vector x + IRO_OFFSET
pub const IRQ_OFFSET: usize = 0x20 + 16;

/// The minimum CPU interrupt vector for IOAPIC or MSI interrupts
pub const VECTOR_MIN: usize = 0;
/// The minimum CPU interrupt vector for IOAPIC or MSI interrupts
pub const VECTOR_MAX: usize = 109;

pub const seL4_NumHWBreakpoints: usize = 4;
pub const seL4_FirstBreakpoint: isize = -1;
pub const seL4_NumExclusiveBreakpoints: usize = 0;
pub const seL4_FirstWatchpoint: isize = -1;
pub const seL4_NumExclusiveWatchpoints: usize = 0;
pub const seL4_FirstDualFunctionMonitor: isize = 0;
pub const seL4_NumDualFunctionMonitors: usize = 4;

pub type seL4_X86_ASIDControl = seL4_CPtr;
pub type seL4_X86_ASIDPool = seL4_CPtr;
pub type seL4_X86_IOSpace = seL4_CPtr;
pub type seL4_X86_IOPort = seL4_CPtr;
pub type seL4_X86_Page = seL4_CPtr;
pub type seL4_X86_PageDirectory = seL4_CPtr;
pub type seL4_X86_PageTable = seL4_CPtr;
pub type seL4_X86_IOPageTable = seL4_CPtr;
pub type seL4_X86_EPTPML4 = seL4_CPtr;
pub type seL4_X86_EPTPDPT = seL4_CPtr;
pub type seL4_X86_EPTPD = seL4_CPtr;
pub type seL4_X86_EPTPT = seL4_CPtr;
pub type seL4_X86_VCPU = seL4_CPtr;

error_types!(u32);

pub const Default_VMAttributes: usize = 0;

macro_rules! ipc_offset_of {
    ($field:ident) => {
        &(*(0 as *const seL4_IPCBuffer)).$field as *const _ as usize
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum seL4_ObjectType {
    seL4_UntypedObject = 0,
    seL4_TCBObject,
    seL4_EndpointObject,
    seL4_NotificationObject,
    seL4_CapTableObject,
    seL4_SchedContextObject,
    seL4_ReplyObject,
    seL4_X86_4K,
    seL4_X86_LargePageObject,
    seL4_X86_PageTableObject,
    seL4_X86_PageDirectoryObject,
    #[cfg(feature = "CONFIG_IOMMU")]
    seL4_X86_IOPageTableObject,
    #[cfg(feature = "CONFIG_VTX")]
    seL4_X86_VCPUObject,
    #[cfg(feature = "CONFIG_VTX")]
    seL4_X86_EPTPML4Object,
    #[cfg(feature = "CONFIG_VTX")]
    seL4_X86_EPTPDPTObject,
    #[cfg(feature = "CONFIG_VTX")]
    seL4_X86_EPTPDObject,
    #[cfg(feature = "CONFIG_VTX")]
    seL4_X86_EPTPTObject,
}

#[repr(u32)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum seL4_X86_VMAttributes {
    WriteBack = 0,
    WriteThrough = 1,
    CacheDisabled = 2,
    Uncacheable = 3,
    WriteCombining = 4,
}

impl Default for seL4_X86_VMAttributes {
    fn default() -> seL4_X86_VMAttributes {
        seL4_X86_VMAttributes::WriteBack
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UserContext {
    pub eip: seL4_Word,
    pub esp: seL4_Word,
    pub eflags: seL4_Word,
    pub eax: seL4_Word,
    pub ebx: seL4_Word,
    pub ecx: seL4_Word,
    pub edx: seL4_Word,
    pub esi: seL4_Word,
    pub edi: seL4_Word,
    pub ebp: seL4_Word,
    pub tls_base: seL4_Word,
    pub fs: seL4_Word,
    pub gs: seL4_Word,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_VCPUContext {
    pub eax: seL4_Word,
    pub ebx: seL4_Word,
    pub ecx: seL4_Word,
    pub edx: seL4_Word,
    pub esi: seL4_Word,
    pub edi: seL4_Word,
    pub ebp: seL4_Word,
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_VMFault {
    pub ip: seL4_Word,
    pub addr: seL4_Word,
    pub prefetch_fault: seL4_Word,
    pub fsr: seL4_Word,
}

impl seL4_VMFault {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_VMFault, isize) {
        (
            seL4_VMFault {
                ip: seL4_GetMR(index),
                addr: seL4_GetMR(index + 1),
                prefetch_fault: seL4_GetMR(index + 2),
                fsr: seL4_GetMR(index + 3),
            },
            index + 4,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UnknownSyscall {
    pub eax: seL4_Word,
    pub ebx: seL4_Word,
    pub ecx: seL4_Word,
    pub edx: seL4_Word,
    pub esi: seL4_Word,
    pub edi: seL4_Word,
    pub ebp: seL4_Word,
    pub fault_ip: seL4_Word,
    pub sp: seL4_Word,
    pub flags: seL4_Word,
    pub syscall: seL4_Word,
}

impl seL4_UnknownSyscall {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_UnknownSyscall, isize) {
        (
            seL4_UnknownSyscall {
                eax: seL4_GetMR(index),
                ebx: seL4_GetMR(index + 1),
                ecx: seL4_GetMR(index + 2),
                edx: seL4_GetMR(index + 3),
                esi: seL4_GetMR(index + 4),
                edi: seL4_GetMR(index + 5),
                ebp: seL4_GetMR(index + 6),
                fault_ip: seL4_GetMR(index + 7),
                sp: seL4_GetMR(index + 8),
                flags: seL4_GetMR(index + 9),
                syscall: seL4_GetMR(index + 10),
            },
            index + 11,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UnknownSyscallReply {
    pub eax: seL4_Word,
    pub ebx: seL4_Word,
    pub ecx: seL4_Word,
    pub edx: seL4_Word,
    pub esi: seL4_Word,
    pub edi: seL4_Word,
    pub ebp: seL4_Word,
    pub eip: seL4_Word,
    pub esp: seL4_Word,
    pub eflags: seL4_Word,
}

impl seL4_UnknownSyscallReply {
    pub unsafe fn to_ipcbuf(&self, index: isize) -> isize {
        seL4_SetMR(index, self.eax);
        seL4_SetMR(index + 1, self.ebx);
        seL4_SetMR(index + 2, self.ecx);
        seL4_SetMR(index + 3, self.edx);
        seL4_SetMR(index + 4, self.esi);
        seL4_SetMR(index + 5, self.edi);
        seL4_SetMR(index + 6, self.ebp);
        seL4_SetMR(index + 7, self.eip);
        seL4_SetMR(index + 8, self.esp);
        seL4_SetMR(index + 9, self.eflags);
        index + 10
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UserException {
    pub fault_ip: seL4_Word,
    pub sp: seL4_Word,
    pub flags: seL4_Word,
    pub number: seL4_Word,
    pub code: seL4_Word,
}

impl seL4_UserException {
    pub unsafe fn from_ipcbuf(index: isize) -> (seL4_UserException, isize) {
        (
            seL4_UserException {
                fault_ip: seL4_GetMR(index),
                sp: seL4_GetMR(index + 1),
                flags: seL4_GetMR(index + 2),
                number: seL4_GetMR(index + 3),
                code: seL4_GetMR(index + 4),
            },
            index + 5,
        )
    }
}

#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct seL4_UserExceptionReply {
    pub eip: seL4_Word,
    pub esp: seL4_Word,
    pub eflags: seL4_Word,
}

impl seL4_UserExceptionReply {
    pub unsafe fn to_ipcbuf(&self, index: isize) -> isize {
        seL4_SetMR(index, self.eip);
        seL4_SetMR(index + 1, self.esp);
        seL4_SetMR(index + 2, self.eflags);
        index + 3
    }
}

#[inline(always)]
pub unsafe fn seL4_GetMR(regnum: isize) -> seL4_Word {
    let mr;
    asm!("movl %fs:${1:c}(,$2,0x4), $0" : "=r"(mr) : "i"(ipc_offset_of!(msg)), "r"(regnum) : : "volatile");
    mr
}

#[inline(always)]
pub unsafe fn seL4_SetMR(regnum: isize, value: seL4_Word) {
    asm!("movl $0, %fs:${1:c}(,$2,0x4)" : : "r"(value), "i"(ipc_offset_of!(msg)), "r"(regnum) : "memory" : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_GetUserData() -> seL4_Word {
    let data;
    asm!("movl %fs:${1:c}, $0" : "=r"(data) : "i"(ipc_offset_of!(userData)) : : "volatile");
    data
}

#[inline(always)]
pub unsafe fn seL4_GetIPCBuffer() -> *mut seL4_IPCBuffer {
    seL4_GetUserData() as isize as *mut seL4_IPCBuffer
}

#[inline(always)]
pub unsafe fn seL4_SetUserData(data: seL4_Word) {
    asm!("movl $0, %fs:${1:c}" : : "r"(data), "i"(ipc_offset_of!(userData)) : "memory" : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_GetReserved() -> seL4_Word {
    let data;
    asm!("movl %fs:${1:c}, $0" : "=r"(data) : "i"(ipc_offset_of!(reserved)) : : "volatile");
    data
}

#[inline(always)]
pub unsafe fn seL4_SetReserved(data: seL4_Word) {
    asm!("movl $0, %fs:${1:c}" : : "r"(data), "i"(ipc_offset_of!(reserved)) : "memory" : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_GetBadge(index: isize) -> seL4_CapData {
    let mut badge: seL4_CapData = uninitialized();
    asm!("movl %fs:${1:c}(,$2,0x4), $0" : "=r"(badge.words[0]) : "i"(ipc_offset_of!(caps_or_badges)), "r"(index) : : "volatile");
    badge
}

#[inline(always)]
pub unsafe fn seL4_GetCap(index: isize) -> seL4_CPtr {
    let cptr;
    asm!("movl %fs:${1:c}(,$2,0x4), $0" : "=r"(cptr) : "i"(ipc_offset_of!(caps_or_badges)), "r"(index) : : "volatile");
    cptr
}

#[inline(always)]
pub unsafe fn seL4_SetCap(index: isize, cptr: seL4_CPtr) {
    asm!("movl $0, %fs:${1:c}(,$2,0x4)" : : "r"(cptr), "i"(ipc_offset_of!(caps_or_badges)), "r"(index) : "memory" : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_GetCapReceivePath(
    receiveCNode: *mut seL4_CPtr,
    receiveIndex: *mut seL4_CPtr,
    receiveDepth: *mut seL4_Word,
) {
    if !receiveCNode.is_null() {
        asm!("movl %fs:${1:c}, $0" : "=r"(*receiveCNode) : "i"(ipc_offset_of!(receiveCNode)) : : "volatile");
    }

    if !receiveIndex.is_null() {
        asm!("movl %fs:${1:c}, $0" : "=r"(*receiveIndex) : "i"(ipc_offset_of!(receiveIndex)) : : "volatile");
    }

    if !receiveDepth.is_null() {
        asm!("movl %fs:${1:c}, $0" : "=r"(*receiveDepth) : "i"(ipc_offset_of!(receiveDepth)) : : "volatile");
    }
}

#[inline(always)]
pub unsafe fn seL4_SetCapReceivePath(
    receiveCNode: seL4_CPtr,
    receiveIndex: seL4_CPtr,
    receiveDepth: seL4_Word,
) {
    asm!("movl $0, %fs:${1:c}" : : "r"(receiveCNode), "i"(ipc_offset_of!(receiveCNode)) : "memory" : "volatile");
    asm!("movl $0, %fs:${1:c}" : : "r"(receiveIndex), "i"(ipc_offset_of!(receiveIndex)) : "memory" : "volatile");
    asm!("movl $0, %fs:${1:c}" : : "r"(receiveDepth), "i"(ipc_offset_of!(receiveDepth)) : "memory" : "volatile");
}

#[inline(always)]
unsafe fn x86_sys_send(sys: seL4_Word, mut dest: seL4_Word, info: seL4_Word, mr1: seL4_Word) {
    asm!("pushl %ebp
          pushl %ebx
          movl %esp, %ecx
          movl %edx, %ebx
          leal 1f, %edx
          1:
          sysenter
          popl %ebx
          popl %ebp"
          : "={edx}" (dest) 
          : "{eax}" (sys),
            "{esi}" (info),
            "{edi}" (mr1),
            "{edx}" (dest)
          : "ecx"
          : "volatile");
}

#[inline(always)]
unsafe fn x86_sys_send_null(sys: seL4_Word, mut src: seL4_Word, info: seL4_Word) {
    asm!("pushl %ebp
          pushl %ebx
          movl %esp, %ecx
          movl %edx, %ebx
          leal 1f, %edx
          1:
          sysenter
          popl %ebx
          popl %ebp"
          : "={edx}" (src)
          : "{eax}" (sys),
            "{esi}" (info),
            "{edx}" (src)
          : "ecx"
          : "volatile");
}

#[inline(always)]
unsafe fn x86_sys_recv(
    sys: seL4_Word,
    src: seL4_Word,
    out_badge: *mut seL4_Word,
    out_info: *mut seL4_Word,
    out_mr1: *mut seL4_Word,
    mut reply: seL4_Word,
) {
    asm!("pushl %ebp
          pushl %ebx
          movl %ecx, %ebp
          movl %esp, %ecx
          movl %edx, %ebx
          leal 1f, %edx
          1:
          sysenter
          movl %ebx, %edx
          popl %ebx
          movl %ebp, %ecx
          popl %ebp"
          : "={edx}" (*out_badge)
            "={esi}" (*out_info),
            "={edi}" (*out_mr1),
            "={ecx}" (reply)
          : "{eax}" (sys),
            "{edx}" (src),
            "{ecx}" (reply)
          : "memory"
          : "volatile");
}

#[inline(always)]
unsafe fn x86_sys_send_recv(
    sys: seL4_Word,
    dest: seL4_Word,
    out_badge: *mut seL4_Word,
    info: seL4_Word,
    out_info: *mut seL4_Word,
    in_out_mr1: *mut seL4_Word,
    mut reply: seL4_Word,
) {
    asm!("pushl %ebp
          pushl %ebx
          movl %ecx, %ebp
          movl %esp, %ecx
          movl %edx, %ebx
          leal 1f, %edx
          1:
          sysenter
          movl %ebx, %edx
          popl %ebx
          movl %ebp, %ecx
          popl %ebp"
          : "={esi}" (*out_info)
            "={edi}" (*in_out_mr1),
            "={edx}" (*out_badge)
            "={ecx}" (reply)
          : "{eax}" (sys),
            "{esi}" (info),
            "{edi}" (*in_out_mr1),
            "{edx}" (dest),
            "{ecx}" (reply)
          : "memory"
          : "volatile");
}

#[inline(always)]
unsafe fn x86_sys_nbsend_wait(
    sys: seL4_Word,
    src: seL4_Word,
    out_badge: *mut seL4_Word,
    info: seL4_Word,
    out_info: *mut seL4_Word,
    in_out_mr1: *mut seL4_Word,
    mut reply: seL4_Word,
) {
    asm!("pushl %ebp
          pushl %ebx
          movl %ecx, %ebp
          movl %esp, %ecx
          movl %edx, %ebx
          leal 1f, %edx
          1:
          sysenter
          movl %ebx, %edx
          popl %ebx
          movl %ebp, %ecx
          popl %ebp"
          : "={esi}" (*out_info)
            "={edi}" (*in_out_mr1),
            "={edx}" (*out_badge),
            "={ecx}" (reply)
          : "{eax}" (sys),
            "{esi}" (info),
            "{edi}" (*in_out_mr1),
            "{edx}" (src),
            "{ecx}" (reply)
          : "memory"
          : "volatile");
}

#[inline(always)]
unsafe fn x86_sys_null(sys: seL4_Word) {
    asm!("pushl %ebp
          pushl %ebx
          movl %esp, %ecx
          leal 1f, %edx
          1:
          sysenter
          popl %ebx
          popl %ebp"
          :
          : "{eax}" (sys)
          : "ecx", "edx"
          : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_Send(dest: seL4_CPtr, msgInfo: seL4_MessageInfo) {
    x86_sys_send(SyscallId::Send as seL4_Word, dest, msgInfo.words[0], seL4_GetMR(0));
}

macro_rules! opt_deref {
    ($name:expr) => {
        if !$name.is_null() {
            *$name
        } else {
            0
        }
    }
}

macro_rules! opt_assign {
    ($loc:expr, $val:expr) => {
        if !$loc.is_null() {
            *$loc = $val;
        }
    }
}

#[inline(always)]
pub unsafe fn seL4_SendWithMRs(dest: seL4_CPtr, msgInfo: seL4_MessageInfo, mr0: *mut seL4_Word) {
    x86_sys_send(
        SyscallId::Send as seL4_Word,
        dest,
        msgInfo.words[0],
        if mr0.is_null() { 0 } else { *mr0 },
    );
}

#[inline(always)]
pub unsafe fn seL4_NBSend(dest: seL4_CPtr, msgInfo: seL4_MessageInfo) {
    x86_sys_send(SyscallId::NBSend as seL4_Word, dest, msgInfo.words[0], seL4_GetMR(0));
}

#[inline(always)]
pub unsafe fn seL4_NBSendWithMRs(dest: seL4_CPtr, msgInfo: seL4_MessageInfo, mr0: *mut seL4_Word) {
    x86_sys_send(
        SyscallId::NBSend as seL4_Word,
        dest,
        msgInfo.words[0],
        if mr0.is_null() { 0 } else { *mr0 },
    );
}

#[inline(always)]
pub unsafe fn seL4_Signal(dest: seL4_CPtr) {
    x86_sys_send_null(
        SyscallId::Send as seL4_Word,
        dest,
        seL4_MessageInfo::new(0, 0, 0, 0).words[0],
    );
}

#[inline(always)]
pub unsafe fn seL4_Recv(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0: seL4_Word = uninitialized();

    x86_sys_recv(
        SyscallId::Recv as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut mr0 as *mut _,
        reply,
    );

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_RecvWithMRs(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut msg0: seL4_Word = uninitialized();

    x86_sys_recv(
        SyscallId::Recv as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut msg0,
        reply,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBRecv(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0: seL4_Word = uninitialized();

    x86_sys_recv(
        SyscallId::NBRecv as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut mr0,
        reply,
    );

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_Wait(src: seL4_CPtr, sender: *mut seL4_Word) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0: seL4_Word = uninitialized();

    x86_sys_recv(
        SyscallId::Wait as seL4_Word,
        src,
        &mut badge,
        &mut info.words[0],
        &mut mr0 as *mut _,
        0,
    );

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_WaitWithMRs(
    src: seL4_CPtr,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut msg0: seL4_Word = uninitialized();

    x86_sys_recv(SyscallId::Wait as seL4_Word, src, &mut badge, &mut info.words[0], &mut msg0, 0);

    opt_assign!(mr0, msg0);
    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBWait(src: seL4_CPtr, sender: *mut seL4_Word) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0: seL4_Word = uninitialized();

    x86_sys_recv(SyscallId::NBWait as seL4_Word, src, &mut badge, &mut info.words[0], &mut mr0, 0);

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_Call(mut dest: seL4_CPtr, msgInfo: seL4_MessageInfo) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut mr0 = seL4_GetMR(0);

    x86_sys_send_recv(
        SyscallId::Call as seL4_Word,
        dest,
        &mut dest,
        msgInfo.words[0],
        &mut info.words[0],
        &mut mr0,
        0,
    );

    seL4_SetMR(0, mr0);

    info
}

#[inline(always)]
pub unsafe fn seL4_CallWithMRs(
    mut dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    mr0: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut msg0: seL4_Word = 0;

    if !mr0.is_null() {
        if msgInfo.get_length() > 0 {
            msg0 = *mr0;
        }
    }

    x86_sys_send_recv(
        SyscallId::Call as seL4_Word,
        dest,
        &mut dest,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        0,
    );

    opt_assign!(mr0, msg0);

    info
}

#[inline(always)]
pub unsafe fn seL4_ReplyRecv(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0 = seL4_GetMR(0);

    x86_sys_send_recv(
        SyscallId::ReplyRecv as seL4_Word,
        dest,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut mr0,
        reply,
    );

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_ReplyWaitWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut msg0: seL4_Word = 0;

    if !mr0.is_null() {
        if msgInfo.get_length() > 0 {
            msg0 = *mr0;
        }
    }

    x86_sys_send_recv(
        SyscallId::ReplyRecv as seL4_Word,
        dest,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        reply,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendRecv(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_Word,
    sender: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0 = seL4_GetMR(0);

    // no spare registers on ia32 -> must use ipc buffer
    seL4_SetReserved(dest);

    x86_sys_nbsend_wait(
        SyscallId::NBSendRecv as seL4_Word,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut mr0,
        reply,
    );

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendRecvWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_Word,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
    reply: seL4_CPtr,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut msg0: seL4_Word = 0;

    // no spare registers on ia32 -> must use ipc buffer
    seL4_SetReserved(dest);

    if !mr0.is_null() {
        if msgInfo.get_length() > 0 {
            msg0 = *mr0;
        }
    }

    x86_sys_nbsend_wait(
        SyscallId::NBSendRecv as seL4_Word,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        reply,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendWait(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_Word,
    sender: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0 = seL4_GetMR(0);

    x86_sys_nbsend_wait(
        SyscallId::NBSendWait as seL4_Word,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut mr0,
        dest,
    );

    seL4_SetMR(0, mr0);

    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_NBSendWaitWithMRs(
    dest: seL4_CPtr,
    msgInfo: seL4_MessageInfo,
    src: seL4_Word,
    sender: *mut seL4_Word,
    mr0: *mut seL4_Word,
) -> seL4_MessageInfo {
    let mut info: seL4_MessageInfo = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut msg0: seL4_Word = 0;

    if !mr0.is_null() {
        if msgInfo.get_length() > 0 {
            msg0 = *mr0;
        }
    }

    x86_sys_nbsend_wait(
        SyscallId::NBSendRecv as seL4_Word,
        src,
        &mut badge,
        msgInfo.words[0],
        &mut info.words[0],
        &mut msg0,
        dest,
    );

    opt_assign!(mr0, msg0);
    opt_assign!(sender, badge);

    info
}

#[inline(always)]
pub unsafe fn seL4_Yield() {
    x86_sys_null(SyscallId::Yield as seL4_Word);
    asm!("" ::: "esi", "edi", "memory" : "volatile");
}

#[inline(always)]
pub unsafe fn seL4_VMEnter(vcpu: seL4_CPtr, sender: *mut seL4_Word) -> seL4_Word {
    let mut fault: seL4_Word = uninitialized();
    let mut badge: seL4_Word = uninitialized();
    let mut mr0 = seL4_GetMR(0);

    x86_sys_send_recv(
        SyscallId::VMEnter as seL4_Word,
        vcpu,
        &mut badge,
        0,
        &mut fault,
        &mut mr0,
        0,
    );

    seL4_SetMR(0, mr0);

    if fault == 0 && !sender.is_null() {
        *sender = badge;
    }

    fault
}

#[inline(always)]
#[cfg(feature = "CONFIG_PRINTING")]
pub unsafe fn seL4_DebugPutChar(c: u8) {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;

    x86_sys_send_recv(
        SyscallId::DebugPutChar as seL4_Word,
        c as seL4_Word,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        0,
    );
}

#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugHalt() -> ! {
    x86_sys_null(SyscallId::DebugHalt as seL4_Word);
    asm!("" ::: "esi", "edi", "memory" : "volatile");
    unreachable!()
}

#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugSnapshot() {
    x86_sys_null(SyscallId::DebugSnapshot as seL4_Word);
    asm!("" ::: "esi", "edi", "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugCapIdentify(mut cap: seL4_CPtr) -> u32 {
    let mut unused0 = 0;
    let mut unused1 = 0;

    x86_sys_send_recv(
        SyscallId::DebugCapIdentify as seL4_Word,
        cap,
        &mut cap,
        0,
        &mut unused0,
        &mut unused1,
        0,
    );
    cap as _
}

/// Note: name MUST be NUL-terminated.
#[inline(always)]
#[cfg(feature = "CONFIG_DEBUG_BUILD")]
pub unsafe fn seL4_DebugNameThread(tcb: seL4_CPtr, name: &[u8]) {
    core::ptr::copy_nonoverlapping(
        name.as_ptr() as *mut u8,
        (&mut (*seL4_GetIPCBuffer()).msg).as_mut_ptr() as *mut u8,
        name.len(),
    );
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;

    x86_sys_send_recv(
        SyscallId::DebugNameThread as seL4_Word,
        tcb,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        0,
    );
}

#[inline(always)]
#[cfg(feature = "CONFIG_DANGEROUS_CODE_INJECTION")]
pub unsafe fn seL4_DebugRun(userfn: extern "C" fn(*mut u8), userarg: *mut u8) {
    let userfnptr = userfn as *mut ();
    x86_sys_send_null(
        SyscallId::DebugRun as seL4_Word,
        userfnptr as seL4_Word,
        userarg as seL4_Word,
    );
    asm!("" ::: "edi", "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkResetLog() -> seL4_Word {
    let mut unused0 = 0;
    let mut unused1 = 0;

    let mut ret = 0;

    x86_sys_send_recv(
        SyscallId::BenchmarkResetLog as seL4_Word,
        0,
        &mut ret,
        0,
        &mut unused0,
        &mut unused1,
        0,
    );

    ret
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkFinalizeLog() -> seL4_Word {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut index_ret = 0;

    x86_sys_send_recv(
        SyscallId::BenchmarkFinalizeLog as seL4_Word,
        0,
        &mut index_ret,
        0,
        &mut unused0,
        &mut unused1,
        0,
    );

    index_ret
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkSetLogBuffer(mut frame_cptr: seL4_Word) -> seL4_Word {
    let mut unused0 = 0;
    let mut unused1 = 0;

    x86_sys_send_recv(
        SyscallId::BenchmarkSetLogBuffer as seL4_Word,
        frame_cptr,
        &mut frame_cptr,
        0,
        &mut unused0,
        &mut unused1,
        0,
    );

    frame_cptr
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkNullSyscall() {
    x86_sys_null(SyscallId::BenchmarkNullSyscall as seL4_Word);
    asm!("" ::: "esi", "edi", "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkFlushCaches() {
    x86_sys_null(SyscallId::BenchmarkFlushCaches as seL4_Word);
    asm!("" ::: "esi", "edi", "memory" : "volatile");
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkGetThreadUtilization(tcb: seL4_Word) -> benchmark_track_util {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;

    x86_sys_send_recv(
        SyscallId::BenchmarkGetThreadUtilisation as seL4_Word,
        tcb,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        0,
    );

    benchmark_track_util {
        thread_utilization: seL4_GetMR(0),
        idle_utilization: seL4_GetMR(1),
        total_utilization: seL4_GetMR(2),
    }
}

#[inline(always)]
#[cfg(feature = "CONFIG_ENABLE_BENCHMARKS")]
pub unsafe fn seL4_BenchmarkResetThreadUtilization(tcb: seL4_Word) {
    let mut unused0 = 0;
    let mut unused1 = 0;
    let mut unused2 = 0;

    x86_sys_send_recv(
        SyscallId::BenchmarkResetThreadUtilisation as seL4_Word,
        tcb,
        &mut unused0,
        0,
        &mut unused1,
        &mut unused2,
        0,
    );
}
